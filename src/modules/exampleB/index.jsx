import React, { Component } from 'react'
import axios from 'axios'
import event from 'js/watch'
import { EXAMPLEA_SIGN } from 'src/sign'
import styles from './index.less'

export default class ExampleB extends Component {
    state = {
        detailB: '',
        dataFromA: ''
    }

    componentDidMount() {
        const that = this

        axios.get('/api/detailB').then(function(response) {
            that.setState({
                detailB: response.data.data.text
            })
        })

        event.on(EXAMPLEA_SIGN, res => {
            this.setState({
                dataFromA: res.payload
            })
        })
    }

    render() {
        return (
            <div className={styles.contentB}>
                b:{this.state.detailB}, a:{this.state.dataFromA}
            </div>
        )
    }
}
