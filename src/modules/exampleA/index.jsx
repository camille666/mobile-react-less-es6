import React, { Component } from 'react'
import axios from 'axios'
import event from 'js/watch'
import { EXAMPLEA_SIGN } from 'src/sign'
import styles from './index.less'

export default class ExampleA extends Component {
    state = {
        detailA: ''
    }

    componentDidMount() {
        const that = this
        axios.get('/api/detailA').then(function(response) {
            that.setState({
                detailA: response.data.data.text
            })
        })
        event.emit(EXAMPLEA_SIGN, {
            payload: '这是A组件传过来的值'
        })
    }

    render() {
        return <div className={styles.contentA}>{this.state.detailA}</div>
    }
}
